### Projet EMI - Gestion des émotions pour enfants

## Description
L'application EMI (Émotions pour les Enfants) a été développée avec SwiftUI et Xcode pour offrir aux enfants un outil ludique et interactif pour gérer leurs émotions. EMI vise à aider les enfants à reconnaître, comprendre et exprimer leurs émotions de manière saine.

## Fonctionnalités principales
- **Journal des émotions :** Les enfants peuvent enregistrer leurs émotions quotidiennes à l'aide d'une interface conviviale.
- **Activités interactives :** Des jeux et des activités spécifiques sont intégrés pour aider les enfants à canaliser leurs émotions.
- **Conseils personnalisés :** EMI offre des conseils adaptés à chaque émotion enregistrée, fournissant un soutien émotionnel personnalisé.
- **Suivi des progrès :** Les parents peuvent suivre les progrès émotionnels de leurs enfants et intervenir si nécessaire.


## Technologies utilisées
- Swift
- SwiftUI
- Xcode

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/EMI.git`
2. Ouvrez le projet dans Xcode.
3. Compilez et exécutez l'application sur votre simulateur iOS ou appareil.

## Auteurs
- Chems MEZIOU
